drop database if exists Mobilificio;
create database Mobilificio;
use Mobilificio;

create table oggetto(
id integer auto_increment not null primary key,
nome varchar(150),
descrizione text,
codice varchar(12) unique,
prezzo float
);

create table categoria(
id integer auto_increment not null primary key,
nome varchar(150) not null,
descrizione text,
codice varchar(150) unique
);

create table oggettocategoria(
idOgg integer not null,
idCat integer not null,
foreign key (idOgg) references oggetto(id),
foreign key (idCat) references categoria(id),
primary key(idOgg,idCat)
);


insert into oggetto(nome, descrizione, codice, prezzo) value ("sedia" ," La sedia o seggiola è un mobile su cui una singola persona, o due, possono sedere (cioè appoggiare natiche e schiena). È formata da un piano orizzontale chiamato seduta, da quattro gambe di sostenimento e da uno schienale o spalliera di appoggio. Sia la seduta sia lo schienale possono assumere forme diverse e il numero delle gambe può variare a seconda del tipo di sedia. Più sedie possono essere posizionate attorno a un tavolo.Un mobile simile alla sedia è la poltrona, che è praticamente una sedia rivestita da uno strato morbido." ,"SED123456789" , 100.12);
insert into oggetto(nome, descrizione, codice, prezzo) value ("scrivania" ,"" ,"SCR224671964" , 250.12);
insert into oggetto(nome, descrizione, codice, prezzo) value ("cassettone" ,"" ,"CAS563871458" , 499.99);
insert into oggetto(nome, descrizione, codice, prezzo) value ("vaso" ,"" ,"VAS126794164" , 124.00);
insert into oggetto(nome, descrizione, codice, prezzo) value ("letto" ,"" ,"LET127890146" , 1000.00);
insert into oggetto(nome, descrizione, codice, prezzo) value ("tazza" ,"" ,"TAZ780125185" , 12.00);
insert into oggetto(nome, descrizione, codice, prezzo) value ("scarpiera" ,"" ,"SCA127392619" , 25.00);
insert into oggetto(nome, descrizione, codice, prezzo) value ("lampadina" ,"" ,"LAM331245981" , 2.00);



insert into categoria(nome, descrizione, codice) value("seduta","parte di un elemento di arredamento, sopra la quale ci si siede, con tale nome vengono anche intesi tutti gli elementi d'arredo (domestico o urbano) concepiti per sedersi (panche, sedie, poltrone ecc.. ecc..). Seduta è a tutti gli effetti un sinonimo di sedile","CAT123456");
insert into categoria(nome, descrizione, codice) value("oggettistica","Produzione industriale o artigianale di oggetti complementari all'arredamento o di oggetti da regalo; anche, l'assortimento di tali oggetti.","CAT054333");
insert into categoria(nome, descrizione, codice) value("illuminazione","L'illuminazione è il risultato dell'illuminare mediante l'utilizzo di flussi luminosi, naturali (mediati da elementi architettonici) o emessi da sorgenti artificiali (apparecchiature generalmente elettriche) allo scopo di ottenere determinati livelli di luce (illuminamenti) sull'oggetto (in senso lato) da illuminare. La relativa tecnica si chiama illuminotecnica. Ulteriori scopi dell'illuminazione sono anche creare effetti scenografici, di accento e con le sue apparecchiature generatrici (lampade) di fare arredo. Il termine illuminazione è anche usato come semplificazione e con significato di impianto di illuminazione.","CAT067431");
insert into categoria(nome, descrizione, codice) value("camera da letto","Insieme di oggetti che possono trovarsi in una camera da letto","CAT32145");
insert into categoria(nome, descrizione, codice) value("cucina","But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?","CAT125622");
insert into categoria(nome, descrizione, codice) value("mobilia","L’insieme dei mobili che costituiscono l’arredamento stabile di una casa (letti, armadî, tavoli, sedie, divani, poltrone, ecc.)","CAT234122");


insert into oggettocategoria(idOgg, idCat) value (1,1);
insert into oggettocategoria(idOgg, idCat) value (1,4);
insert into oggettocategoria(idOgg, idCat) value (1,5);
insert into oggettocategoria(idOgg, idCat) value (2,4);
insert into oggettocategoria(idOgg, idCat) value (2,6);
insert into oggettocategoria(idOgg, idCat) value (3,4);
insert into oggettocategoria(idOgg, idCat) value (3,6);
insert into oggettocategoria(idOgg, idCat) value (4,2);
insert into oggettocategoria(idOgg, idCat) value (5,4);
insert into oggettocategoria(idOgg, idCat) value (5,6);
insert into oggettocategoria(idOgg, idCat) value (6,5);
insert into oggettocategoria(idOgg, idCat) value (6,2);
insert into oggettocategoria(idOgg, idCat) value (7,6);
insert into oggettocategoria(idOgg, idCat) value (8,2);
insert into oggettocategoria(idOgg, idCat) value (8,3);

select * from categoria
join oggettocategoria on categoria.id = oggettocategoria.idCat
where oggettocategoria.idOgg = 1;


