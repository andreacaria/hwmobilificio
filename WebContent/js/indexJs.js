/**
 * 
 */
$(document).ready(
	function(){
		aggiornaTab();
	}
	
);

function aggiornaTab(){
	$.ajax(
		{
			url:"http://localhost:8080/HwMobilificio/aggiornatab",
			method:"POST",
			success: function(risultato){
				table(risultato);
			}
			
		}
	);
	
}
function table(risultato){
	var strTable = "";
	for(let i=0; i<risultato.length;i++){
		strTable += stampaRiga(risultato[i]);
	}
	$("#TabProd").html(strTable);
	
}
function stampaRiga(iOgg){
	// occhio è un JSON non un oggetto non puoi usare i metodi getter and setter
	//let StringRow = "<tr data-identificatore = '"+iArt.id+"''>";
	let StringRow = "<tr data-codice='"+iOgg.codice+"'>";
	StringRow += "	<td>"+iOgg.codice+"</td>";
	StringRow += "	<td>"+iOgg.nome+"</td>";
	if(iOgg.descrizione.length>180)
		StringRow += "	<td>"+iOgg.descrizione.substr(0,180)+"... </td>";
	
	else
		StringRow += "	<td style='mid-width: 500px'>"+iOgg.descrizione+"</td>";
	StringRow += "	<td>"+iOgg.prezzo+"</td>";
	
	
	StringRow += "	<td><button type='button' class='btn btn-outline-warning btn-block' onclick='cardCategoria(this)'>Mostra</button></td>";
	StringRow += "</tr>";
	
	return StringRow;	
}

function cardCategoria(objButton){
	let CODselezionato = $(objButton).parent().parent().data("codice");
	$.ajax(
		{
			url:"http://localhost:8080/HwMobilificio/categoriesingoloogg",
			method:"POST",
			data: {
					codice: CODselezionato
				},
			success: function(risultato){
				createModalCard(risultato);
				$("#modalTitle").html('<h4 class="modal-title card-element-title">'+CODselezionato+'</h4>');
				$("#modaleCat").modal("show");
			}
			
		}
	);
	
}
function createModalCard(risultatJson){
	let modalBody= '<div class="card-content clearfix m-2" >';
	for(let i=0; i<risultatJson.length; i++){
		modalBody+='<div class="card m-2 ">';
		modalBody+='<div class="card-body">';
		modalBody+='	<h5 class="card-title">'+risultatJson[i].nome+'</h5>';
		modalBody+='	<p class="card-text">'+risultatJson[i].descrizione+'</p>';
		modalBody+='</div></div>';	 
		
		
	}
	modalBody += "</div>";
	
	
	
	$("#modalBody").html(modalBody);
	
}

