/**
 * 
 */
$(document).ready(
	function() {

		$("#modPro").click(
			function() {
				modaleModifica();
			}
		)
	}

);

function modaleModifica() {
	$.ajax(
		{
			url: "http://localhost:8080/HwMobilificio/aggiornatab",
			method: "POST",
			success: function(risultato) {
				let strModalBody = corpoModaleModifica(risultato);
				$("#modalBodyModificaProd").html(strModalBody);
				$("#modaleModProd").modal("show");
			}
		}
	)
}

function corpoModaleModifica(risultato) {
	//let modalBody = '<div class="card-content clearfix m-2" >';
	let modalBody = '<div class="list-group m-2">';
	for (let i = 0; i < risultato.length; i++) {
		modalBody += '<button data-codice = "' + risultato[i].codice + '" type="button" onclick="modificaOgetto(this)" class="list-group-item list-group-item-action">' + risultato[i].nome + '- ' + risultato[i].codice + '</button>'
	}
	modalBody += '</div>'
	return modalBody;
}

function modificaOgetto(objButton) {
	let codiceOgg = $(objButton).data("codice");
	console.log(codiceOgg);
	$("#modaleModProd").modal("toggle");
	$.ajax(
		{
			url: "http://localhost:8080/HwMobilificio/categoriesingoloogg",
			method: "POST",
			data: {
				codice: codiceOgg
			},
			success: function(risCat) {
				$.ajax(
					{
						url: "http://localhost:8080/HwMobilificio/singolooggbycod",
						method: "POST",
						data: {
							codice: codiceOgg
						},
						success: function(risOgg) {
							$.ajax(
								{
									url: "http://localhost:8080/HwMobilificio/getallcategorie",
									method: "POST",

									success: function(allCat) {
										let strModalBody = corpoModificaProdottoSelezionato(allCat);
										$("#modalBodyModificaProdSelezionato").html(strModalBody)
										$("#nomeModifica").val(risOgg.nome);
										$("#descrizioneModifica").val(risOgg.descrizione);
										$("#prezzoModifica").val(risOgg.prezzo);

										$("#modaleModProdSelezionato").modal("show");

									}
								}
							);
						}
					}
				);
			}
		}
	)
}

function corpoModificaProdottoSelezionato(allCat) {

	let strCorpo = '<div class="form-group">';
	strCorpo += '<label for="nomeModifica">Nome</label>';
	strCorpo += ' <input type="text" class="form-control" id="nomeModifica"placeholder="Enter Name">';
	strCorpo += '<label for="descrizione">Descrizione</label>';
	strCorpo += '<textarea id = "descrizioneModifica" class="form-control"></textarea>';
	strCorpo += '<label for="prezzoModifica">Prezzo</label>';
	strCorpo += '<input type="float" class="form-control" id="prezzoModifica" placeholder="Enter Price">';
	strCorpo += '<label for="descrizione">Categoria</label>';
	strCorpo += '<ul class="list-group"> ';
	for(let i=0; i<allCat.length; i++){
		strCorpo += '<li class="list-group-item">'+allCat[i].nome+'</li>';	
	}			
		strCorpo += '</ul > ';
		strCorpo += '</div>';
	return strCorpo;

}


