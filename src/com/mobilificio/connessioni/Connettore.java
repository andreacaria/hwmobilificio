package com.mobilificio.connessioni;

import java.sql.Connection;
import java.sql.SQLException;


import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

public class Connettore {
	private Connection conn;
	private static Connettore ogg_connessione;
	
	public static Connettore getIstanza() {
		if(ogg_connessione == null) {
			ogg_connessione = new Connettore();
			return ogg_connessione;
		}
		else {
			return ogg_connessione;
		}
	}
	
	public Connection getConnessione() throws SQLException{
		if(conn == null) {
			MysqlDataSource dataSource = new MysqlDataSource();
			dataSource.setServerName("127.0.0.1");  //equivale a localhost
			dataSource.setPortNumber(3306);
			dataSource.setUser("root");
			dataSource.setPassword("toor");
			dataSource.setUseSSL(false);//settiamo in modo che possa comunicare anche in http senza ssl 
			dataSource.setDatabaseName("Mobilificio");
			dataSource.setAllowPublicKeyRetrieval(true);	//Public key retrieval
			
			conn = dataSource.getConnection();
			return conn;
		}
		else {
			return conn;
		}
	}

}
