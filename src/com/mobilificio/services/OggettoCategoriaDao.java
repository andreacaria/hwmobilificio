package com.mobilificio.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mobilificio.classi.Categoria;
import com.mobilificio.classi.Oggetto;
import com.mobilificio.connessioni.Connettore;

public class OggettoCategoriaDao implements DAOappoggio<Oggetto , Categoria >{

	@Override
	public boolean insert(Oggetto ogg, Categoria cat) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "INSERT oggettocategoria(idOgg, idCat) value (?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setInt(1, ogg.getId());
       	ps.setInt(2, cat.getId());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
       	if(risultato.getInt(1)>0) {
       		return true;
       		
       	}
       	else
       		return false;
		
	}

}
