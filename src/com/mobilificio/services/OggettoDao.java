package com.mobilificio.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mobilificio.classi.Categoria;
import com.mobilificio.classi.Oggetto;
import com.mobilificio.connessioni.Connettore;

public class OggettoDao implements DAO<Oggetto>{
	
	public ArrayList<Categoria> getAllCategorieById(int ID) throws SQLException{
		ArrayList<Categoria> elencoCat = new ArrayList<Categoria>();
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "select * from categoria join oggettocategoria on categoria.id = oggettocategoria.idCat "
				+ "where oggettocategoria.idOgg = ?;";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, ID);
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()) {
       		Categoria temp = new Categoria();
       		temp.setId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setDescrizione(risultato.getString(3));
       		temp.setCodice(risultato.getString(4));
       		
       		elencoCat.add(temp);
       	};
       	return elencoCat;
       	
       	
	}
	@Override
	public Oggetto getByID(int ID) throws SQLException {
   		Connection conn = Connettore.getIstanza().getConnessione();
       	String query = "SELECT id, nome, descrizione, codice, prezzo FROM oggetto WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, ID);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

       	Oggetto temp = new Oggetto();
   		temp.setId(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setDescrizione(risultato.getString(3));
   		temp.setCodice(risultato.getString(4));
   		temp.setPrezzo(risultato.getFloat(5));
       	
       	return temp;
		
	}
	@Override
	public Oggetto getByCOD(String COD) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
       	String query = "SELECT id, nome, descrizione, codice, prezzo FROM oggetto WHERE codice = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, COD);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

       	Oggetto temp = new Oggetto();
       	temp.setId(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setDescrizione(risultato.getString(3));
   		temp.setCodice(risultato.getString(4));
   		temp.setPrezzo(risultato.getFloat(5));
       	
       	return temp;
	}

	@Override
	public ArrayList<Oggetto> getAll() throws SQLException {
		ArrayList<Oggetto> elencoOgg = new ArrayList<Oggetto>();
		Connection conn = Connettore.getIstanza().getConnessione();
       	String query = "SELECT id, nome, descrizione, codice, prezzo FROM oggetto WHERE 1=1";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ResultSet risultato = ps.executeQuery();
       	
       	while(risultato.next()) {
       		Oggetto temp = new Oggetto();
       		temp.setId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setDescrizione(risultato.getString(3));
       		temp.setCodice(risultato.getString(4));
       		temp.setPrezzo(risultato.getFloat(5));
       		elencoOgg.add(temp);
       	}
		
		return elencoOgg;
	}

	@Override
	public boolean insert(Oggetto t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "INSERT INTO oggetto(nome, descrizione, codice, prezzo) VALUES (?,?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getDescrizione());
       	ps.setString(3, t.getCodice());
       	ps.setFloat(4, t.getPrezzo());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
       	if(risultato.getInt(1)>0) {
       		t.setId(risultato.getInt(1));
       		return true;
       		
       	}
       	else
       		return false;
		
	}

	@Override
	public boolean delete(Oggetto t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "DELETE FROM oggetto WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public boolean update(Oggetto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}



}
