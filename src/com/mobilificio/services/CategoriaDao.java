package com.mobilificio.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mobilificio.classi.Categoria;
import com.mobilificio.classi.Oggetto;
import com.mobilificio.connessioni.Connettore;

public class CategoriaDao implements DAO<Categoria>{

	@Override
	public Categoria getByID(int ID) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
       	String query = "SELECT id, nome, descrizione, codice FROM oggetto WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, ID);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

       	Categoria temp = new Categoria();
   		temp.setId(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setDescrizione(risultato.getString(3));
   		temp.setCodice(risultato.getString(4));
   		
       	return temp;
	}
	public Categoria getByCOD(String Cod) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
       	String query = "SELECT id, nome, descrizione, codice FROM oggetto WHERE codice= ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, Cod);
       	
       	ResultSet risultato = ps.executeQuery();
       	risultato.next();

       	Categoria temp = new Categoria();
       	temp.setId(risultato.getInt(1));
   		temp.setNome(risultato.getString(2));
   		temp.setDescrizione(risultato.getString(3));
   		temp.setCodice(risultato.getString(4));
   		
       	return temp;
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {
		ArrayList<Categoria> elencoOgg = new ArrayList<Categoria>();
		Connection conn = Connettore.getIstanza().getConnessione();
       	String query = "SELECT id, nome, descrizione, codice FROM categoria WHERE 1=1";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ResultSet risultato = ps.executeQuery();
       	
       	while(risultato.next()) {
       		Categoria temp = new Categoria();
       		temp.setId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setDescrizione(risultato.getString(3));
       		temp.setCodice(risultato.getString(4));
       		
       		elencoOgg.add(temp);
       	}
		
		return elencoOgg;
	}

	@Override
	public boolean insert(Categoria t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "INSERT INTO categoria(nome, descrizione, codice) VALUES (?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getDescrizione());
       	ps.setString(3, t.getCodice());
    
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
       	if(risultato.getInt(1)>0) {
       		t.setId(risultato.getInt(1));
       		return true;
       		
       	}
       	else
       		return false;
	}

	@Override
	public boolean delete(Categoria t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "DELETE FROM categoria WHERE id = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setInt(1, t.getId());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public boolean update(Categoria t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

}
