package com.mobilificio.services;

import java.sql.SQLException;
import java.util.ArrayList;

public interface DAO<T> {
	
	T getByID(int ID) throws SQLException;
	T getByCOD(String COD) throws SQLException;
	ArrayList<T> getAll() throws SQLException;
	boolean insert(T t) throws SQLException;
	boolean delete(T t) throws SQLException;
	boolean update(T t) throws SQLException;
	
	
	

}
