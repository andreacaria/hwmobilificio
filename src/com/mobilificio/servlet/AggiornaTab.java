package com.mobilificio.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.classi.Categoria;
import com.mobilificio.classi.Oggetto;
import com.mobilificio.services.CategoriaDao;
import com.mobilificio.services.OggettoDao;

/**
 * Servlet implementation class AggiornaTab
 */
@WebServlet("/aggiornatab")
public class AggiornaTab extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		OggettoDao oggDao = new OggettoDao();
		CategoriaDao catDao = new CategoriaDao();
		Gson Json = new Gson();
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		try {
			ArrayList<Oggetto> elencoOgg = oggDao.getAll();
			for(Oggetto iOgg: elencoOgg ) {
				ArrayList<Categoria> iCatTemp = oggDao.getAllCategorieById(iOgg.getId());
				iOgg.setCategorie(iCatTemp);
			}
			String elencoOggJson = Json.toJson(elencoOgg);
			out.print(elencoOggJson);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRORE DB:" + e.getMessage());
		}
		
		
		
	}

}
