package com.mobilificio.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.classi.Categoria;
import com.mobilificio.classi.Oggetto;
import com.mobilificio.services.CategoriaDao;
import com.mobilificio.services.OggettoDao;

/**
 * Servlet implementation class GetAllCategorie
 */
@WebServlet("/getallcategorie")
public class GetAllCategorie extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		CategoriaDao catDao = new CategoriaDao();
		Gson Json = new Gson();
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		try {
			
			
			ArrayList<Categoria> iCatElenco= catDao.getAll();
			String elencoOggJson = Json.toJson(iCatElenco);
			out.print(elencoOggJson);
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("ERRORE DB:" + e.getMessage());
		}
		
		
		
	}
		
	

}
