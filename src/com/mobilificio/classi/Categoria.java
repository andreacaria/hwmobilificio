package com.mobilificio.classi;

public class Categoria  {
	private int id;
	private String nome;
	private String codice;
	private String descrizione;
	
	public Categoria() {
		
	}
	public Categoria(int id, String nome, String codice, String descrizione) {
		super();
		this.id = id;
		this.nome = nome;
		this.codice = codice;
		this.descrizione = descrizione;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
}
