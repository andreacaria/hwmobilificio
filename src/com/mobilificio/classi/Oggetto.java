package com.mobilificio.classi;

import java.util.ArrayList;

public class Oggetto {
	private int id;
	private String nome;
	private String codice;
	private String descrizione;
	private Float prezzo;
	private ArrayList<Categoria> categorie;
	
	public ArrayList<Categoria> getCategorie() {
		return categorie;
	}
	public void setCategorie(ArrayList<Categoria> categorie) {
		this.categorie = categorie;
	}
	public Oggetto(int id, String nome, String codice, String descrizione, Float prezzo) {
		super();
		this.id = id;
		this.nome = nome;
		this.codice = codice;
		this.descrizione = descrizione;
		this.prezzo = prezzo;
	}
	public Oggetto() {
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public Float getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(Float prezzo) {
		this.prezzo = prezzo;
	}
	
}
